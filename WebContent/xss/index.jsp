<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%
String ME = request.getRequestURI() + "?";
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>XSS sample</title>
</head>
<body>
	<h1>XSS - sample</h1>
    <form action="<%=ME%>">
        <label for="fname">First name:</label>
        <input type="text" id="fname" name="fname"><br><br>
        <label for="lname">Last name:</label>
        <input type="text" id="lname" name="lname"><br><br>
        <input type="submit" value="Submit">
    </form>
</body>
</html>